﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletController : MonoBehaviour {

    SpawnerController spawner;

    // Use this for initialization
    void Start ()
    {
        spawner = SpawnerController.GetInstance();
        StartCoroutine("StartBallBahaviour");

    }
	
	// Update is called once per frame
	void Update ()
    {
        this.GetComponent<Rigidbody2D>().velocity = transform.right;
    }

    IEnumerator StartBallBahaviour()
    {
        yield return new WaitForSeconds(Random.Range( 0.25f, 1));
        spawner.AddTower(transform.position);
        spawner.RemoveBullet(this.gameObject);
    }
    
    void OnCollisionEnter2D(Collision2D coll)
    {
        if(coll.gameObject.tag == "Tower")
        {
            StopCoroutine("StartTowerBahaviour");
            spawner.RemoveTower(coll.gameObject);
            spawner.RemoveBullet(this.gameObject);
        }
    }

    void OnEnable()
    {
        this.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
        StartCoroutine("StartBallBahaviour");
    }

}
