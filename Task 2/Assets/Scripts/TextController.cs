﻿using UnityEngine;
using UnityEngine.UI;

public class TextController : MonoBehaviour {
    public Text text;
    public string sentence = "Towers: ";

    static TextController instance;

    public static TextController GetInstance()
    {
        return instance;
    }
    
    public void TowersCount(int count)
    {
        text.text = sentence + count;
    }

    // Use this for initialization
    void Awake () {

        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Debug.Log("Singleton object called " + name + " was destroied, because exist different object of this type.");
            Destroy(this.gameObject);
        }
    }
	
	// Update is called once per frame
	void Update () {
		
	}


}
