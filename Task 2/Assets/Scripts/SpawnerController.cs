﻿using System.Collections.Generic;
using UnityEngine;

public class SpawnerController : MonoBehaviour {

    // ---- Towers ----
    public GameObject prefabTower;
    public int TowerCount = 100;
    public Queue<GameObject> poolTower;
    public List<GameObject> activeTower;

    // ---- Bullets ----
    public GameObject prefabBullet;
    public int bulletsCount = 400;
    public Queue<GameObject> poolBullets;
    public List<GameObject> activeBullets;

    TextController textController;
    bool endGame = false;

    static SpawnerController instance;

    public static SpawnerController GetInstance()
    {
        return instance;
    }

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Debug.Log("Singleton object called " + name + " was destroied, because exist different object of this type.");
            Destroy(this.gameObject);
        }

    }

    // Use this for initialization
    void Start ()
    {
        poolTower = new Queue<GameObject>();
        poolBullets = new Queue<GameObject>();
        activeTower = new List<GameObject>();
        textController = TextController.GetInstance();
        textController.TowersCount(activeTower.Count);

        for (int i = 0; i < TowerCount; i++)
        {
            GameObject obj = Instantiate(prefabTower);
            obj.transform.parent = transform;
            obj.SetActive(false);
            poolTower.Enqueue(obj);
        }

        for (int i = 0; i < bulletsCount; i++)
        {
            GameObject obj = Instantiate(prefabBullet);
            obj.transform.parent = transform;
            obj.SetActive(false);
            poolBullets.Enqueue(obj);
        }

        AddTower(transform.position);
	}
	
    public void AddTower(Vector3 position)
    {
        if(endGame)
        {
            return;
        }

        if (activeTower.Count < TowerCount)
        {
            GameObject obj = poolTower.Dequeue();
            obj.transform.position = position;
            obj.transform.rotation = Quaternion.identity;
            obj.SetActive(true);
            activeTower.Add(obj);
            textController.TowersCount(activeTower.Count);
        } else 
        {
            endGame = true;
            for(int i = 0; i < activeTower.Count; i++)
            {
                activeTower[i].GetComponent<TowerController>().Restart();
            }
        }
    }

    public void RemoveTower(GameObject obj)
    {
        activeTower.Remove(obj);
        obj.SetActive(false);
        poolTower.Enqueue(obj);
        textController.TowersCount(activeTower.Count);
    }

    public void AddBullet(Vector3 position, Vector3 rotation)
    {
        GameObject obj = poolBullets.Dequeue();
        obj.transform.rotation = Quaternion.identity;
        obj.transform.position = position;
        obj.transform.Rotate(rotation);
        obj.SetActive(true);
        activeBullets.Add(obj);
    }

    public void RemoveBullet(GameObject obj)
    {
        activeBullets.Remove(obj);
        obj.SetActive(false);
        poolBullets.Enqueue(obj);
    }

    // Update is called once per frame
    void Update () {
		
	}

}
