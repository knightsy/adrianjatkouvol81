﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerController : MonoBehaviour {

    // ---- Turret ----
    public SpriteRenderer turretSprite;
    public float firstDelay = 6;
    float rotateAfter = 0.5f;
    float minRotationAngle = 15;
    float maxRotationAngle = 45;
    Vector3 nextRotation = Vector3.zero;

    // ---- Bullet ----
    public Transform buletSpawnPoint;
    float minBulletRange = 1;
    float maxBulletRange = 4;
    int bulletCounter = 12;

    SpawnerController spawner;

    // Use this for initialization
    void Start ()
    {
        spawner = SpawnerController.GetInstance();
    }
	
	// Update is called once per frame
	void Update () {
        
	}

    void RotateTurret()
    {
        nextRotation.z = Random.Range(minRotationAngle, maxRotationAngle);
        transform.Rotate(nextRotation);
    }

    void Shoot()
    {
        spawner.AddBullet(buletSpawnPoint.position, transform.rotation.eulerAngles);
    }

    public void Restart()
    {
        turretSprite.color = Color.white;
        StopCoroutine("StartTowerBahaviour");
        StartCoroutine("StartTowerBahaviour");
    }
    
    IEnumerator StartTowerBahaviour()
    {
        yield return new WaitForSeconds(firstDelay);
        turretSprite.color = Color.red;
        for (int i = 0; i < bulletCounter; i++)
        {
            yield return new WaitForSeconds(rotateAfter);
            RotateTurret();
            Shoot();
        }
        turretSprite.color = Color.white;

    }

    void OnEnable()
    {
        turretSprite.color = Color.white;
        StartCoroutine("StartTowerBahaviour");
    }

}
