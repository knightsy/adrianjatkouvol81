﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class MoveOnClick : MonoBehaviour {
    public float power = 10;
    Rigidbody rigidbody;
    Vector3 powerVector;

	// Use this for initialization
	void Start () {
        rigidbody = GetComponent<Rigidbody>();
        powerVector = Vector3.zero;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    
    void OnMouseDrag()
    {
        powerVector.x = Input.GetAxis("Mouse X") * power;
        rigidbody.velocity = powerVector;
    }

}
